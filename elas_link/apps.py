from django.apps import AppConfig


class ElasLinkConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'elas_link'
