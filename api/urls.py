from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('is-logged-in/', IsLoggedInView.as_view()),
    path('create-deck/', CreateDeckView.as_view()),
    path('create-user/', UserViewSet.as_view()),
    path('get-user-decks', GetUserDecks.as_view()),
    path('deck-practice', CreateQuizzes.as_view()),
    path('get-deck-data', GetDeckData.as_view()),
    path('duplicate-deck/', DuplicateDeckView.as_view()),
    path('get-game-deck/', GetGameDeck.as_view()),
    path('save-to-deck/', SavetoDeck.as_view()),
    path('search-user-deck/', SearchUserDeck.as_view()),
    path('user-leaderboard/', PopularUserView.as_view()),
    path('deck-leaderboard/', PopularDeckView.as_view()),
    path('edit-word/', EditWord.as_view()),
    path('delete-word/', DeleteWord.as_view()),
    path('change-avatar/', ChangeAvatar.as_view()),
    path('get-elastic-link/', Elaslink.as_view()),
]

