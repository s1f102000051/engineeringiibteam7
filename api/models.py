from django.db import models
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.core.validators import MinLengthValidator
import string, random

# Create your models here.
def generate_unique_code():
    length = 6
    while True:
        code = ''.join(random.choices(string.ascii_uppercase, k = length))
        if Deck.objects.filter(deckID=code).count() == 0:
            break
    return code

class MyAccountManager(BaseUserManager):
    def create_user(self, username, password=None):
        if not username:
            raise ValueError("Username is required")

        user = self.model(
            username = username,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, username, password):
        user = self.create_user(
            username = username,
            password = password
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

# https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#django.contrib.auth.models.AbstractBaseUser

class Word(models.Model):
    text = models.CharField(default="", max_length=150)
    definition = models.CharField(default="", max_length=150)
    jlpt_level = models.IntegerField(default=5)
    added_at = models.DateTimeField(default = timezone.now)
    deckID = models.CharField(max_length=6,null=True, blank=True)
    def __str__(self):
        return self.text

class Deck(models.Model):
    wordList = models.ManyToManyField(Word)
    deckName = models.CharField(max_length=50, default="Deck JLPT")
    owner = models.CharField(max_length=20) # username
    deckID = models.CharField(max_length=6, default=generate_unique_code, unique=True)
    duplicate_time = models.IntegerField(default=0)
    def __str__(self):
        return "%s %s" % (self.deckName, self.deckID)

class User(AbstractBaseUser):
    username = models.CharField(max_length=20, validators=[MinLengthValidator(6)], unique=True)
    deckList = models.ManyToManyField(Deck)
    description = models.CharField(default = "Hello, welcome to my page.", max_length=150)
    duplicate_time = models.IntegerField(default=0)
    avatar = models.ImageField(upload_to='upload/userAvatar/',null=True, blank=True)

    experience_level = models.IntegerField(default=0)
    is_admin = models.BooleanField(default = False)
    is_active = models.BooleanField(default = True)
    is_staff = models.BooleanField(default = False)
    is_superuser = models.BooleanField(default = False)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELD = ['username']
    objects = MyAccountManager() 
    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return True
    def EditProfile():
        pass
    def addFriend():
        pass





