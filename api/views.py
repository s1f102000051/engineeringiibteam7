from django.core.validators import ProhibitNullCharactersValidator
from django.shortcuts import render
from rest_framework import generics, serializers, status
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework.views import APIView
from django.forms.models import model_to_dict
from rest_framework.authtoken.models import Token
from django.contrib.auth.decorators import login_required
from .models import *
from api.serializer import *
import random

from django.db.models import CharField
from django.db.models.functions import Lower
from django.db.models.functions import Lower
from django.core.paginator import Paginator
CharField.register_lookup(Lower)
# Create your views her e.


class IsLoggedInView(APIView):
    def post(self, request, format=None):
        if request.user.is_authenticated:
            username = self.request.user.username
            curUser = User.objects.get(username=username)
            # print(self.request.user)
            response = {
                'username': username,
                'curUser' : UserSerializer(curUser).data
            }
            return Response(response, status=status.HTTP_200_OK)
        return Response({'username': ''}, status=status.HTTP_200_OK)


class CreateDeckView(APIView):
    serializer_class = CreateDeckSerializer

    # @login_required
    def post(self, request, format=None):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()

        serializer = self.serializer_class(data=request.data)
        print("Create deck data: ", request.data)
        if (serializer.is_valid()):
            deckName = serializer.data.get('deckName')
            owner = self.request.user.username
            deck = Deck(deckName=deckName, owner=owner)
            deck.save()
            request.user.deckList.add(deck)
            request.user.experience_level += 1
            request.user.save()
            return Response(DeckSerializer(deck).data, status=status.HTTP_201_CREATED)
        return Response({'Bad Request': 'Invalid data...'}, status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(generics.CreateAPIView):  # just for testing, does not work now
    querySet = User.objects.all()
    serializer_class = UserSerializer

    # def post(self, request, *args, **kwargs):



class SavetoDeck(APIView):
    serializer_class = AddToDeck

    def post(self, request, format=None):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()
        print(request.data)

        serializer = self.serializer_class(data=request.data)
        if (serializer.is_valid()):
            deckID = serializer.data.get('deckID')
            definition = serializer.data.get('definition')
            text = serializer.data.get('text')
            word = Word(definition=definition, text=text, deckID=deckID)
            word.save()
            curDeck = request.user.deckList.get(deckID=deckID)
            curDeck.wordList.add(word)
            return Response({'Success': "Added to deck"}, status=status.HTTP_201_CREATED)
        return Response({'Bad Request': 'Invalid data...'}, status=status.HTTP_400_BAD_REQUEST)

class EditWord(APIView):
    serializer_class = WordSerializer

    def post(self, request):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()
        
        serializer = self.serializer_class(data=request.data)
        print(request.data)
        if (serializer.is_valid()):
            # Get current deck
            deckID = serializer.data.get('deckID')
            curDeck = request.user.deckList.get(deckID=deckID)
            # Get current word
            id = request.data['id']
            curWord = curDeck.wordList.get(id = id)
            # Update current word
            definition = serializer.data.get('definition')
            text = serializer.data.get('text')
            added_at = timezone.now()
            curWord.definition = definition
            curWord.text = text
            curWord.added_at = added_at
            curWord.save()
            return Response({'Success': "Edited"}, status=status.HTTP_201_CREATED)
        return Response({'Failed': 'Invalid data...'}, status=status.HTTP_400_BAD_REQUEST)

class DeleteWord(APIView):
    serializer_class = WordSerializer

    def delete(self, request):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()
    
        serializer = self.serializer_class(data=request.data)
        print(request.data)
        if (serializer.is_valid()):
            # Get current deck
            deckID = request.data['deckID']
            curDeck = request.user.deckList.get(deckID=deckID)
            # Get current word
            id = request.data['id']
            curWord = curDeck.wordList.get(id = id)
            # Delete current word
            curWord.delete()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




class GetUserDecks(APIView):

    lookup_url_kwarg = 'username'

    def get(self, request, format=None):
        username = request.GET.get(self.lookup_url_kwarg)
        if username != None:
            # print("username: ",username)
            viewingUser = User.objects.get(username=username)
            decks = []
            for deck in viewingUser.deckList.all():
                decks.append({'name': deck.deckName, 'id': deck.deckID, 'wordsList': [
                            WordSerializer(word).data for word in deck.wordList.all()]})

            decks.reverse()
            # print(decks)
            return Response({'decks': decks, 'viewingUser': UserSerializer(viewingUser).data}, status=status.HTTP_200_OK)
        return Response({'Bad Request': 'username parameter not found in request'}, status=status.HTTP_400_BAD_REQUEST)


items = []


def check_num_of_words():
    global items
    print("checking.........", items)
    # in case the word in database is not sufficient for creating quiz
    if (len(list(Word.objects.all())) < 10):
        newWord = Word(text="何方へ", definition="where to?")
        newWord.save()
        newWord = Word(text="ドジ", definition="clumsiness")
        newWord.save()
        newWord = Word(text="ドタバタ", definition="slapstick")
        newWord.save()
        newWord = Word(text="に当たって", definition="in light of")
        newWord.save()

    items = list(Word.objects.all())
    return items


def create_quiz(word):
    global items
    if (len(items) < 10):
        items = check_num_of_words()
    # print("_items: ", items)
    words_dict = {}
    number_of_selections = 4
    while len(words_dict) < number_of_selections - 1:
        randomWord = random.choice(items)
        randText = randomWord.text
        randDef = randomWord.definition
        # print("random word:", randText, randDef)
        try:
            print("Already existed word: ",
                  words_dict[randText], "-- debug for creating quiz")
        except:
            if word.definition != randDef:
                words_dict[randText] = randDef
    random_items = []
    for key, value in words_dict.items():
        random_items.append({'text': key, 'definition': value})
    # print("quiz: ",random_items)
    return random_items


class CreateQuizzes(APIView):
    lookup_url_kwarg = 'deck'

    def get(self, request, format=None):
        deckID = request.GET.get(self.lookup_url_kwarg)
        data = []
        if deckID != None:
            currentDeck = Deck.objects.get(deckID=deckID)
            # print(currentDeck.deckName)
            # wordsList =[{'text': word.text, 'definition': word.definition} for word in currentDeck.wordList.all()]
            for word in currentDeck.wordList.all():
                quiz = create_quiz(word)
                quiz = [item['definition'] for item in quiz]
                # generate a random answer in range (1,4) for the quiz
                pos = random.randint(1, 4)
                quiz.insert(pos-1, word.definition)
                quiz_packet = {'question': word.text,
                               'selections': quiz, 'answer': pos}
                data.append(quiz_packet)
            # print(data)

            # print(decks)
            return Response({'data': data, 'deckName': currentDeck.deckName}, status=status.HTTP_200_OK)
        return Response({'Bad Request': 'username parameter not found in request'}, status=status.HTTP_400_BAD_REQUEST)

# For game room at /deck/game


class GetGameDeck(APIView):
    lookup_url_kwarg = 'deck'

    def get(self, request, format=None):
        deckID = request.GET.get(self.lookup_url_kwarg)
        if (deckID != None):
            currentDeck = Deck.objects.get(deckID=deckID)
            deckList = [[quiz.text, quiz.definition]
                        for quiz in currentDeck.wordList.all()]
            if (len(deckList) < 10):
                return Response({'Bad Request': 'You deck must have at least 10 words'}, status=status.HTTP_400_BAD_REQUEST)
            selectedDecklist = random.sample(deckList, 10)
            print(deckList)
            # Add two side of the words
            data = []
            for id, [text, definition] in enumerate(selectedDecklist):
                # id -> For unique key.
                data += [{'text': text, 'id': id * 2,
                          'isClicked': False, 'isDeleted': False}]
                data += [{'text': definition, 'id': id * 2 +
                          1, 'isClicked': False, 'isDeleted': False}]
            random.shuffle(data)  # Random order
            return Response({'data': data}, status=status.HTTP_200_OK)
        return Response({'Bad Request': 'Bad request'}, status=status.HTTP_400_BAD_REQUEST)


class GetDeckData(APIView):
    lookup_url_kwarg = 'deckID'

    def get(self, request, format=None):
        deckID = request.GET.get(self.lookup_url_kwarg)
        data = []
        if deckID != None:
            currentDeck = Deck.objects.get(deckID=deckID)
            owner = User.objects.get(username=currentDeck.owner)
            # print(owner.id)
            # for word in currentDeck.wordList.all():
            #     print(word.text)
            print(UserSerializer(owner).data)

            return Response({'name': currentDeck.deckName, 'id': currentDeck.deckID, 'wordsList': [model_to_dict(word) for word in currentDeck.wordList.all()], 'owner': UserSerializer(owner).data}, status=status.HTTP_200_OK)
        return Response({'Bad Request': 'currentDeck parameter not found in request'}, status=status.HTTP_400_BAD_REQUEST)


class DuplicateDeckView(APIView):
    serializer_class = DuplicateDeckSerializer
    # print("API is running")
    # @login_required

    def post(self, request, format=None):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()
        # print(request.data)
        deckID = request.data['deckID']
        duplicatedDeck = Deck.objects.get(deckID=deckID)
        prev_owner = User.objects.get(username = duplicatedDeck.owner)
        print("prev_owner", prev_owner, prev_owner.experience_level)
        # print("Duplicate deck data: ", duplicatedDeck.wordList, duplicatedDeck.deckName)
        newDeck = {
            'deckName': 'Copy of ' + duplicatedDeck.deckName, 'owner': self.request.user.username, 'deckID': generate_unique_code(),
        }

        # print("New Deck: ", newDeck)

        serializer = self.serializer_class(data=newDeck)
        # print(serializer)
        if (serializer.is_valid()):  # check if it's OK to input it in DB
            print("valid!!")
            deckName = serializer.data.get('deckName')
            deckID = serializer.data.get('deckID')
            owner = self.request.user.username
            
            newDeck = Deck(deckName=deckName, owner=owner)
            newDeck.save()
            for word in duplicatedDeck.wordList.all():
                newWord = Word(text=word.text, definition=word.definition)
                newWord.save()
                newDeck.wordList.add(newWord)

            print("Deck added: ", newDeck)
            request.user.deckList.add(newDeck)
            # This is for increasing duplicate record.
            duplicatedDeck.duplicate_time += 1
            self.request.user.duplicate_time += 1
            self.request.user.save()
            duplicatedDeck.save()
            if (prev_owner.username != owner):
                prev_owner.experience_level += 2
                prev_owner.save()
            # DeckSerializer(deck).data
            return Response("Success", status=status.HTTP_201_CREATED)
        return Response({'Bad Request': 'Invalid data...'}, status=status.HTTP_400_BAD_REQUEST)


class SearchUserDeck(APIView):
    lookup_url_kwarg = 'q'

    def get(self, request, format=None):
        searchContent = request.GET.get(self.lookup_url_kwarg)
        # print(searchContent == None)
        data = []
        if searchContent != None:
            # print(searchContent)
            user_results = list(User.objects.filter(
                username__lower__contains=searchContent))
            deck_results = list(Deck.objects.filter(
                deckName__lower__contains=searchContent))
            print("User: ", user_results)
            print("Deck: ", deck_results)
            return Response({"users": [UserSerializer(user).data for user in user_results], "decks": [DeckSerializer(deck).data for deck in deck_results]}, status=status.HTTP_200_OK)
        # else: #get all the data, not using
        #     user_results = list(User.objects.all())[:5]
        #     deck_results = list(Deck.objects.all())[:5]
        #     data = {"users": [{'label': user.username, 'description': user.description} for user in user_results],
        #             "decks": [DeckSerializer(deck).data for deck in deck_results]}
        #     print(data)
        #     return Response(data, status=status.HTTP_200_OK)
        return Response({'Bad Request': 'currentDeck parameter not found in request'}, status=status.HTTP_400_BAD_REQUEST)

class PopularUserView(APIView):
    def post(self, request, format=None):
        leaderboard = User.objects.all().order_by('-duplicate_time')
        PAGE_SIZE = 16
        paginator = Paginator(leaderboard, PAGE_SIZE)
        curPage = int(request.data.get('userPage'))
        res = []
        if curPage <= paginator.num_pages:
            data = paginator.get_page(curPage)
            res = [UserSerializer(d).data for d in data]

        if (leaderboard != None):
            return Response({'data' : res}, status=status.HTTP_200_OK)
        return Response({'Bad request' : 'There is no data'}, status=status.HTTP_400_BAD_REQUEST)

class PopularDeckView(APIView):
    def post(self, request, format=None):
        leaderboard = Deck.objects.all().order_by('-duplicate_time')
        PAGE_SIZE = 16
        paginator = Paginator(leaderboard, PAGE_SIZE)
        curPage = int(request.data.get('deckPage'))
        res = []
        if curPage <= paginator.num_pages:
            data = paginator.get_page(curPage)
            res = [DeckSerializer(d).data for d in data]

        if (leaderboard != None):
            return Response({'data' : res}, status=status.HTTP_200_OK)
        return Response({'Bad request' : 'There is no data'}, status=status.HTTP_400_BAD_REQUEST)

class ChangeAvatar(APIView):
    # @login_required
    def post(self, request, format=None):
        # if not self.request.session.exists(self.request.session.session_key):
        #     self.request.session.create()
        try:
            curUser = User.objects.get(username=request.data['username'])
            print(curUser.avatar)
            curUser.avatar = request.data['Avatar']
            curUser.save()
            print("Change Ava's data: ",request.data)
            return Response("Successful uploaded", status=status.HTTP_201_CREATED)
        except:
            return Response({'Bad Request': 'Cannot upload this avatar'}, status=status.HTTP_400_BAD_REQUEST)

class Elaslink(APIView):
    # @login_required
    def get(self, request, format=None):
        
        
        try:
            return Response({'Curlink': request.session['elastic_link']}, status=status.HTTP_200_OK)
        except:
            return Response({'Bad Request': 'Cannot get current Elastic link'}, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()
        
       
        try:
            if request.session['elastic_link'] != request.data['savedLink']:
                request.session['elastic_link'] = request.data['savedLink']
                print("passed link: ", request.session['elastic_link'])
            else:
                request.session['elastic_link'] = None
            savedLink = request.data['savedLink']
            print(savedLink)
            return Response("Successful uploaded", status=status.HTTP_201_CREATED)
        except:
            return Response({'Bad Request': 'Cannot upload this avatar'}, status=status.HTTP_400_BAD_REQUEST)

