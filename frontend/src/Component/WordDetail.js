import React from 'react'
import {Grid, Button, Typography} from '@material-ui/core'
import Popup from './Popup'
import {useState} from 'react'
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
export default function WordDetail({wordDetail, word}) {
  const [buttonpopup, setbuttonpopup] = useState(false)

  return (
    <div>
      <Grid container className ="def-wrapper">
        <div style={{width: '98%', paddingLeft: '2%', padding: '20px'}} >
          <Typography variant = "h4" style = {{borderBottom: '1px solid black', width : 'fit-content'}}className='definition-text'>Definitions</Typography>
          {/* <hr style={{margin: '0px'}} /> */}
        </div>
        {wordDetail.gloss.map((glosses) => ( 
          <Grid item xs = {6} style = {{ width : '46%', paddingLeft: '4%'}}key={glosses} className="worddetail-def">
            {/* <FiberManualRecordIcon style = {{fontSize: '10px'}}/>{glosses} */}
            <li>{glosses}</li>
          </Grid>
        ))}
      </Grid>
    </div>
  )
}
