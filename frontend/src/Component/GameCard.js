import React from 'react'
import {Button} from '@material-ui/core'
import { Grid } from '@material-ui/core'
export default function GameCard(props) {
  const styles = {
    width: '20%',
    height: '200px',
    backgroundColor: props.isClicked ? '#FFC106' : '#FEE17F',
    opacity : props.isDeleted ? '0' : '1'
  }
  return (
      <Button variant="outlined" style={styles} onClick={props.handleOnclick}>
        {props.value}
      </Button>
  )
}
