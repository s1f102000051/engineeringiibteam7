import React, {Component, useState, useEffect} from 'react'
import InputBase from '@material-ui/core/InputBase'
import {Grid, Button, Typography} from '@material-ui/core'
import {alpha, makeStyles} from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search'
import {useHistory} from 'react-router-dom'
import {positions} from '@mui/system'
import Zoom from 'react-reveal/Zoom'
//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    background: 'pink',
    position: 'relative',
    backgroundColor: alpha(theme.palette.common.white, 0.75),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.black, 0.05),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
    transition: '0.3s',
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '150px',
    [theme.breakpoints.up('sm')]: {
      width: '150px',
      '&:focus': {
        width: '330px',
      },
    },
  },
}))
export default function SearchBar() {
  const [searchKeywords, setsearchKeywords] = useState('')
  const [searchResult, setsearchResult] = useState([])
  const classes = useStyles()
  const history = useHistory()
  const [suggestBoxClass, setSuggestBoxClass] = useState('disappear-div') //display suggest box or not

  //notify if searching by API
  const [isAPI, setIsAPI] = useState(false)
  const [isAPINotified, setIsAPINotified] = useState(false)

  if (isAPI && !isAPINotified) {
    Notification({
      message:
        'We cannot maintain an Elasticsearch server continuously, so we are using Jisho API instead.  😔 😔 😔 ',
      container: 'top-right',
      title: 'Elasticsearch server is not working now!',
      type: 'info',
      animationIn: ['animate__animated', 'animate__bounceIn'],
      dismiss: {
        duration: 15000,
        pauseOnHover: true,
        onScreen: true,
      },
    })
    setIsAPINotified(true)
  }

  useEffect(() => {
    // console.log("Search bar re-render")
    if (searchKeywords) { // searching word cannot be empty 
      // console.log('searching for: ', searchKeywords)
      const timeoutID = setTimeout(() => {
        console.log("Search for :", searchKeywords)
        // Send Axios request here
        fetch(`/search?word=${searchKeywords}`)
        .then((response) => response.json())
        .then((data) => {
          setsearchResult(
            data['ans'].map((res, index) => (
              <Zoom duration={200}>
                <Button
                  key={index}
                  className="suggest-item"
                  onClick={() => {
                    history.push(`/word?q=${res.text}`)
                    history.go(0)
                  }}
                >
                  <div className={'suggest-item-text'}>{res.text}</div>
                  <div className={'suggest-item-definition'}>{res.gloss.join(', ')}</div>
                </Button>{' '}
              </Zoom>
            )),
          )
          if (!data['api']) setIsAPI(false)
          else {
            setIsAPI(true)
          }
        })
      }, 300)
      return () => {
        console.log("The search results of this keyword will be ignored:", searchKeywords)
        clearTimeout(timeoutID)
      }
    }
  }, [searchKeywords])
  return (
    <Grid container>
      <Grid item xs={11}>
        <div className={classes.search} style={{borderRadius: '30px'}}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            className="searchInput"
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            onFocus={(e) => {
              setSuggestBoxClass('')
            }}
            onBlur={(e) => {
              setTimeout(() => {
                setSuggestBoxClass('disappear-div')
              }, 200)
            }}
            inputProps={{'aria-label': 'search'}}
            onChange={(event) => setsearchKeywords(event.target.value)}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                history.push(`/word?q=${searchKeywords}`)
                history.go(0)
              }
            }}
          />
        </div>

        <div className={classes.search}>
          <div className={`suggest-box ${suggestBoxClass}`}>{searchResult}</div>
        </div>
        {/* <div style={{ marginLeft: '10px', color: 'gray', fontSize:'15px'}} >
          {isAPI && "We cannot maintain an Elasticsearch server anytime, so we are using Jisho API instead. ( However, we are still working on this, so there are many bugs existing :( We will fix it ASAP :> ) "} 
        </div> */}
      </Grid>
      <Grid item xs={1}>
        <Button
          className="search-button"
          style={{backgroundColor: '#FEE17F', borderRadius: '30px', marginLeft: '10px'}}
          variant="contained"
          onClick={() => {
            history.push(`/word?q=${searchKeywords}`)
            history.go(0)
          }}
        >
          Search
        </Button>
      </Grid>
    </Grid>
  )
}
