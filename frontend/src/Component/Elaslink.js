import React from 'react'
import {useState, useEffect, useContext} from 'react'
import {UserContext} from './UserContext'
import {Grid, Typography} from '@material-ui/core'
import NavigationBar from './NavigationBar'
import {Button, Card} from 'react-bootstrap'
import {getCookie} from '../helper'

//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'

export default function Elaslink() {
  const {username, setusername} = useContext(UserContext)
  // console.log('hello')
  const [curElaslink, setCurElasLink] = useState('')
  const [isModifying, setIsModifying] = useState(false)
  useEffect(() => {
    return fetch('/api/get-elastic-link')
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        try {
          console.log(data)
          setCurElasLink(data.Curlink)
        } catch (error) {
          console.log('Error: ', error)
        }
      })
  }, [])

  function handleChange(event) {
    console.log(event)
    const {name, value, type, checked} = event.target
    setCurElasLink(event.target.value)
  }

  function handleSubmit(event) {
    setIsModifying(!isModifying)
    console.log("Saved link: ",curElaslink)
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        savedLink: curElaslink
      }),
    }
    fetch('/api/get-elastic-link/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        console.log("Change successfully")
      })
  }
  return (
    <div>
      <ReactNotification />
      <NavigationBar></NavigationBar>
      <div className="content">
        

        <Grid item>
          {/* <div>
            {isModifying ? "true" : "false"}
          </div> */}
          <div>Current Elastic link: {!isModifying && curElaslink}
          <span style={{display: isModifying ? '' : 'none'}}>
            <input
              type="text"
              placeholder="Elasticsearch Link"
              onChange={handleChange}
              name="elasLink"
              value={curElaslink}
            />
          </span>
          </div>
          <Button
            variant="primary"
            onClick={() => {
              setIsModifying(!isModifying)
            }}
            style={{display: !isModifying ? '' : 'none'}}
          >
            {' '}
            Change{' '}
          </Button>{' '}
         
          <Button
            variant="primary"
            onClick={handleSubmit}
            style={{display: isModifying ? '' : 'none'}}
          >
            {' '}
            Submit{' '}
          </Button>{' '}
        </Grid>
       
      </div>
    </div>
  )
}
