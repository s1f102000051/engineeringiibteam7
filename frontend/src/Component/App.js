import React, {Component} from 'react'
import {render} from 'react-dom'
import HomePage from './HomePage'
import WordPage from './WordPage'
import Logout from '../Auth/Logout'
import Member from './Member'
import Practice from './PracticePage'
import DeckView from './DeckView'
import Elaslink from './Elaslink'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'
import Signup from '../Auth/Signup'
import ForgotPassword from '../Auth/ForgotPassword'
import {UserContext} from './UserContext'
import {useState, useMemo} from 'react'
import Game from './Game'
import LeaderBoard from './LeaderBoard'
export default function App() {
  // console.log("This is running")
  const [username, setusername] = useState('')
  const value = useMemo(() => ({username, setusername}), [username, setusername])
  return (
    <Router>
      <Switch>
        <UserContext.Provider value={value}>
          <Route exact path="/" component={HomePage}></Route>
          <Route path="/word" component={WordPage} />
          <Route path="/member" component={Member} />
          <Route path="/popular" component={LeaderBoard} />
          <Route exact path="/deck" component={DeckView} />
          <Route exact path="/deck/practice" component={Practice} />
          <Route exact path="/deck/game" component={Game} />
          <Route path="/elastic" component={Elaslink} />
        </UserContext.Provider>
      </Switch>
    </Router>
  )
}
const appDiv = document.getElementById('app')
render(<App />, appDiv)
