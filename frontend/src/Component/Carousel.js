//Todo:
//Fix add to deck button
//Fix search bar
//Make edit
//Make gif(jisho API)
//Make jlpt level(jisho API)

import makeCarousel from 'react-reveal/makeCarousel'
import Slide from 'react-reveal/Slide'
import React, {useState} from 'react'
import styled, {css} from 'styled-components'
import Flashcard from './Flashcard'
import {Button} from '@material-ui/core'

export default function Carousel(props) {
  const final = {
    text: 'Good Job!',
    definition: <Button variant="contained">Restart</Button>,
  }
  if (!props.word.length)
    final.text = 'No cards'
  const width = '100%',
    height = 'auto'
  const Container = styled.div`
    position: relative;
    overflow: hidden;
    width: ${width};
    heigh: ${height};
    text-align: center;
    padding: 5px;
  `
  const Children = styled.div`
    width: ${width};
    position: relative;
    height: ${height};
    z-index: 90;
    padding: 3px;
    margin-bottom: 8px;
    margin-top: -3px
  `
  const Arrow = styled.div`
    text-shadow: 1px 1px 1px #fff;
    z-index: 100;
    line-height: ${height};
    text-align: center;
    position: absolute;
    top: 175px;
    width: 10%;
    font-size: 2.5em;
    cursor: pointer;
    user-select: none;
    padding-bottom: 20px;
    ${(props) =>
      props.right
        ? css`
            left: 85%;
          `
        : css`
            left: 2%;
          `}
    &:hover {
      color: rgba(165, 42, 42, 1);
    }
  `
  const Dot = styled.span`
    font-size: 1.5em;
    cursor: pointer;
    text-shadow: 1px 1px 1px #fff;
    user-select: none;
  `
  const Dots = styled.span`
    position: absolute;
    text-align: center;
    width: ${width};
    z-index: 100;
    bottom:50px;
    left: 0px;
  `
  const CarouselUI = ({position, total, handleClick, children}) => (
    <Container>
      <Children>
        {children}
        <Arrow onClick={handleClick} data-position={position - 1}>
          {'<'}
        </Arrow>
        <Arrow right onClick={handleClick} data-position={position + 1}>
          {'>'}
        </Arrow>
      </Children>
      <Dots>
        {Array(...Array(total)).map((val, index) => (
          <Dot key={index} onClick={handleClick} data-position={index}>
            {index === position ? '● ' : '○ '}
          </Dot>
        ))}
      </Dots>
    </Container>
  )
  const Carousel = makeCarousel(CarouselUI)

  //defaultWait 0 will cause the page to throw an error, set to close to infinity instead'
  //It will cause very weird bugs, I tried, don't bother, I wasted 4 hours
  return (
    <Carousel defaultWait={100000000000000}>
      {props.word.map((card, index) => {
        return (
          <Slide right duration={300} key={index}>
            <div>
              <Flashcard word={card} />
            </div>
          </Slide>
        )
      })}
      <Slide right duration={300}>
        <div className="last">
          <p>{final.text}</p>
          <Button
            style={{
              backgroundColor: 'rgb(66, 85, 255)',
              padding: '20px 20px 20px 20px',
              display: `${props.word.length ? 'flex' : 'none'}`,
              color: 'rgba(255, 255, 255)',
              '&:hover': {
                background: 'white',
                color: 'white',
              },
            }}
            
            disableElevation
            variant="contained"
            href={`../deck/practice?deckID=${props.curDeck}`}
          >
            {' '}
            Practice with this deck{' '}
          </Button>
        </div>
      </Slide>
    </Carousel>
  )
}
