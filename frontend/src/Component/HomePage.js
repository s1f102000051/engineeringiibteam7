import React from 'react'
import {Button, Grid, Typography} from '@material-ui/core'
import NavigationBar from './NavigationBar'
import {useState} from 'react'
import Image from 'material-ui-image'
import BuildIcon from '@mui/icons-material/Build'
import SentimentVerySatisfiedIcon from '@mui/icons-material/SentimentVerySatisfied'
import PeopleOutlineIcon from '@mui/icons-material/PeopleOutline'
import FacebookIcon from '@mui/icons-material/Facebook'
import GitHubIcon from '@mui/icons-material/GitHub'
import YouTubeIcon from '@mui/icons-material/YouTube'
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import Fade from 'react-reveal/Fade'
import Rotate from 'react-reveal/Rotate'

import Zoom from 'react-reveal/Zoom'

export default function HomePage() {
  const [view, setview] = useState('0')
  const img = [
    // 'https://i.ibb.co/vH66pt5/270819759-449964973242757-8377810677795873140-n.jpg',
    'https://media.discordapp.net/attachments/917654405690564622/930291639648391199/unknown.png?width=1524&height=740',
    'https://cdn.discordapp.com/attachments/917654405690564622/930316198879576104/Picture1.png',
    'https://media.discordapp.net/attachments/917654405690564622/930318610390466580/unknown.png?width=1890&height=740',
  ]
  return (
    <div>
      <NavigationBar></NavigationBar>

      <div className="content">
        <Grid container spacing={1}>
          <Grid item xs={4} style={{border: '2px'}}>
            <h1 className="homepage-title text-4xl md:text-6xl font-bold">
              <span>
                <img src={'/static/logo.png'} height="70px" width="160px" />
              </span>
              Superpower
            </h1>
            <div className="introduction">
              <div className={`homepage-intro${view == '0' ? '--select' : ''}`}>
                <Grid
                  align="center"
                  style={{width: '100%'}}
                  onClick={() => {
                    setview(0)
                  }}
                >
                  <BuildIcon />
                  <Typography variant="h6">SMART TOOLS</Typography>
                  {view == '0' ? (
                    <Fade>
                      <div>
                        Our intelligent tools know where you need to focus and what you've mastered.
                        Improve your skills easily.
                      </div>
                    </Fade>
                  ) : (
                    <div></div>
                  )}
                </Grid>
              </div>

              <div className={`homepage-intro${view == '1' ? '--select' : ''}`}>
                <Grid
                  align="center"
                  style={{width: '100%'}}
                  onClick={() => {
                    setview(1)
                  }}
                >
                  <SentimentVerySatisfiedIcon />
                  <Typography variant="h6">FUN TO LEARN</Typography>
                  {view == '1' ? (
                    <Fade>
                      <div>
                        Create your custom learning deck and play fun quiz to test your knowledge
                      </div>
                    </Fade>
                  ) : (
                    <div></div>
                  )}
                </Grid>
              </div>

              <div className={`homepage-intro${view == '2' ? '--select' : ''}`}>
                <Grid
                  align="center"
                  style={{width: '100%'}}
                  onClick={() => {
                    setview(2)
                  }}
                >
                  <PeopleOutlineIcon />
                  <Typography variant="h6">LEARN WITH OTHERS</Typography>
                  {view == '2' ? (
                    <Fade>
                      <div>
                        Join over many Japanese students in your learning journey. Get assistance
                        from others and our Japanese instructors quickly.
                      </div>
                    </Fade>
                  ) : (
                    <div></div>
                  )}
                </Grid>
              </div>
            </div>
          </Grid>
          <Grid item xs={8}>
            {/* <Image style={{width: '100%'}} src={img[view]}></Image> */}
            <Fade spy={view} bottom right>
              <img style={{width: '100%'}} src={img[view]}></img>
            </Fade>
          </Grid>
        </Grid>
        
      </div>
      <div className="information">
          <Grid container spacing={1}>
            <Grid item xs={3} align="center">
              <Typography variant="h5">Contact Information</Typography>
              <Typography variant="body1">
                We are a team of young developer with enthusiasm in making things happens
              </Typography>

              <a style={{color: 'white', margin: '5px'}} href="https://github.com/tuyentran0500">
                <GitHubIcon />
              </a>
              <a style={{color: 'white', margin: '5px'}} href="https://www.linkedin.com/in/vinh-dang-b3b239236/">
                <LinkedInIcon />
              </a>
              <a style={{color: 'white', margin: '5px'}} href="https://gitlab.com/s1f102000039">
                <GitHubIcon />
              </a>
            </Grid>
            <Grid item xs={9} align="right">
              <Typography variant="h5" margin style={{marginRight:'75px'}}>Information</Typography>
              <a style={{color: 'white', textDecoration: 'none', marginRight:'100px'}} href="/about-us">
                About Us
              </a>
            </Grid>
          </Grid>
        </div>
    </div>
  )
}
