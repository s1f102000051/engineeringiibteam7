import React from 'react'
import {getCookie} from '../helper'
import {Link, useHistory} from 'react-router-dom'

//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'

export default function DuplicateDeck( deck, history, username){
    // console.log(deck)
    
    console.log("Duplicating", deck, " request from ")
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        deckID : deck,
      }),
    }
    
    fetch('/api/duplicate-deck/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        console.log(data)
        // console.log(deckName)
        if (data=='Success'){
          localStorage.removeItem('curDeck');
          history.push(`/member/?username=${username}#duplicatedsuccess`) //success notification
          history.go(0)
        }
        else{
          Notification({message:'Cannot duplicate this deck', container: "bottom-left", title:'Failed', type: 'danger', animationIn: ["animate__animated", "animate__bounceIn"] })
        }
        
      })
}