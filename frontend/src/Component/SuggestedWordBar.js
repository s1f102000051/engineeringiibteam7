import React from 'react'
import {Grid} from '@material-ui/core'
import Box from '@mui/material/Box'

export default function SuggestedWordBar({suggestedWords}) {
  return (
    <Box className = "suggested--bar">
      <h2 className = "related-search">Related Searches</h2> 
      {suggestedWords}
    </Box>
  )
}
