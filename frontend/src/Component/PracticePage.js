import React from 'react'
import {useLocation} from 'react-router-dom'
import {useState, useEffect} from 'react'
import {Grid, Typography} from '@material-ui/core'
import NavigationBar from './NavigationBar'
import SearchBar from './SearchBar'
import SuggestedWordBar from './SuggestedWordBar'
import {Button, Alert} from 'react-bootstrap'
import Confetti from 'react-confetti'
import Ads from './Ads'
import {getCookie} from '../helper'
import Quiz from './Quiz'
import {CSSTransition} from 'react-transition-group'
import Jump from 'react-reveal/Jump'

export default function NewQuiz() {
  const [username, setusername] = useState('')
  const [token, settoken] = useState('')

  // get params from url
  const searchParam = useLocation().search
  const [curDeck, setCurDeck] = useState(new URLSearchParams(searchParam).get('deckID')) 

  const [data, setData] = useState([]) // save all the data needed for this page

  // for transition
  const [showMessage, setShowMessage] = useState(false)

  // const [userAnswer, setUserAnswer] = useState([]) //can be used for optimizing code in the future
  const [quizzesElements, setQuizzesElements] = useState([])
  const [curAnswer, setCurAnswer] = useState([]) // set the default after getting quizzes data
  const [score, setScore] = useState(-1) // if score!=-1 => submitted

  // get user data
  // useEffect(() => {
  //   // console.log('This is running')
  //   const csrftoken = getCookie('csrftoken')
  //   const requestOptions = {
  //     method: 'POST',
  //     headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
  //   }

  //   fetch('/api/is-logged-in/', requestOptions)
  //     .then((res) => res.json())
  //     .then((data) => {
  //       setusername(data.username)
  //       // console.log(username)
  //     })
  // }, [])

  function handleChange(event) {
    const {name, value, type, checked} = event.target
    // console.log(parseInt(name), parseInt(value), type, checked)
    setCurAnswer((prevResult) => {
      let newResult = [...prevResult]
      newResult[parseInt(name)] = parseInt(value)
      // console.log(newResult)
      return newResult
    })
  }

  function handleSubmit(event) {
    event.preventDefault()
    const timer = setTimeout(() => {
      setShowMessage(true)
    }, 1000)
    // console.log(curAnswer)
    setScore(0)
    for (var i = 0; i < data.length; i++) {
      // console.log("i: ",i,"answer: ",data[i].answer)
      if (curAnswer[i] == data[i].answer) setScore((score) => score + 1)
    }
    // console.log('Score:', score)
  }

  useEffect(() => {
    fetch('/api/deck-practice' + '?deck=' + curDeck)
      .then((response) => {
        if (!response.ok) {
          console.log('No deck')
        }
        return response.json()
      })
      .then((data) => {
        setData(data.data)
        setCurAnswer(Array(data.data.length).fill(0)) // set the default Answer
        setCurDeck(data.deckName) // change from ID to Name
      })
  }, [])

  useEffect(() => {
    setQuizzesElements(
      data.map((quiz, index) => {
        // console.log(index)
        let background_color = ''
        if (score!=-1){
          console.log("curAns: ", curAnswer[index], "answer: ", data[index].answer)
          if (curAnswer[index]=== data[index].answer)
            background_color = 'springgreen'  // correct
          else if (curAnswer[index]===0)
            background_color = 'rgba(128, 128, 128, 0.804)'  // no answer
          else 
            background_color = 'rgba(255, 0, 0, 0.5)' // wrong
        }
        return (
          <Quiz
            quiz_background_color={background_color}
            key={index}
            score={score}
            answer={quiz.answer}
            curAnswer={curAnswer}
            quiz_index={index}
            handleChange={handleChange}
            question={quiz.question}
            selections={quiz.selections}
            index={index}
          />
        )
      }),
    )
  }, [curAnswer, score])

  function scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    })
  }

  function saveScore() {
    console.log(score)
  }

  return (
    <div>
      <NavigationBar></NavigationBar>
      <div className="content">
        {curAnswer.length === score && <Confetti style={{position: 'fixed'}} className='confetti'/>}
        <Grid item xs={10} className="quiz-content" style={{marginLeft: '10%'}}>
          <div className="quiz-title">Quizzes for {curDeck} </div>
          {showMessage &&
          <Jump count={2}>
          
              <Alert
                variant="info"
                className="score--alert"
                dismissible
                onClose={() => setShowMessage(false)}
              >
                <div>
                  {curAnswer.length === score ? (
                    <Alert.Heading
                      style={{fontSize: '40px', fontWeight: '1000', color: 'forestgreen'}}
                      className="quiz--score"
                    >
                      Full score!!!!!!!!!!!!!
                    </Alert.Heading>
                  ) : (
                    <Alert.Heading
                      style={{fontSize: '40px', fontWeight: '1000', color: 'forestgreen'}}
                      className="quiz--score"
                    >
                      Your score: {score}/{curAnswer.length}
                    </Alert.Heading>
                  )}
                </div>
                {/* <div>
                  <Button
                    onClick={saveScore}
                    className="save-score--button"
                    variant="outline-primary"
                  >
                    Save
                  </Button>
                </div> */}
              </Alert>

          </Jump>
          }

          <form
            style={{display: 'flex', flexDirection: 'column'}}
            onSubmit={handleSubmit}
            id="userAnswer"
          >
            {quizzesElements}
            <Button
              type="submit"
              form="userAnswer"
              className="button--submit"
              variant="outline-primary"
              disabled={score != -1}
              onClick={scrollToTop}
            >
              Submit
            </Button>
          </form>
        </Grid>
      </div>
    </div>
  )
}
