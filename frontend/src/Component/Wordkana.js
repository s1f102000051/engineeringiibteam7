import React from 'react'
import {useState} from 'react'

function Wordkana({wordDetail, word}) {
  console.log("word Detail: ",wordDetail)
  return (
    <div>
      <div className="worddetail">
        <p className="worddetail-kanji">{wordDetail.text}, </p>
        <p className="worddetail-kana">{wordDetail.text_kana}</p>
      </div>
    </div>
  )
}

export default Wordkana
