import React from 'react'
import {useState, useEffect, useContext} from 'react'
import {useLocation} from 'react-router-dom'
import {Grid, Typography} from '@material-ui/core'
import NavigationBar from './NavigationBar'
import SearchBar from './SearchBar'
import SuggestedWordBar from './SuggestedWordBar'
import {Link, useHistory} from 'react-router-dom'
import {Button, Card} from 'react-bootstrap'
import {CSSTransition} from 'react-transition-group'
import Ads from './Ads'
import DuplicateDeck from './DuplicateDeck'
import UserCard from './UserCard'
import {getCookie} from '../helper'
import {UserContext} from './UserContext'
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Carousel from './Carousel'
//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'
import Editlist from './Editlist'

export default function DeckView() {
  const {username, setusername} = useContext(UserContext) // logged in account
  const history = useHistory()
  // get params from url
  const searchParam = useLocation().search
  const [curDeck, setCurDeck] = useState(new URLSearchParams(searchParam).get('deckID'))

  const [data, setData] = useState()
  const [curDeckElements, setCurDeckElements] = useState([])
  const [owner, setOwner] = useState({})

  // fetch deck data with username
  useEffect(() => {
    // console.log('viewingDeck: ', curDeck)
    if (curDeck)
      return fetch('/api/get-deck-data' + '?deckID=' + curDeck)
        .then((response) => {
          if (!response.ok) {
            console.log('No deck')
          }
          return response.json()
        })
        .then((data) => {
          try {
            setData(data)
            setOwner(data.owner)
            setCurDeck(data.name)
            setCurDeckElements(
              data.wordsList.map((item, index) => {
                // console.log(item.text)
                return {
                  text: item.text,
                  definition: item.definition,
                  id: item.id,
                  deckID: item.deckID,
                }
              }),
            )
          } catch (error) {
            console.log('Cannot find the data for this deck: ', curDeck) // change to notification in the future
          }
        })
  }, [])

  

  function handleDuplicate(){
    DuplicateDeck(data.id, history, username)
  }

  return (
    <div>
      <NavigationBar></NavigationBar>
      <div className="content">
        <Grid item xs={11} style={{marginRight: '10%'}} className="deck-container">
          <div className="deck-title">{curDeck}</div>

          <div><Editlist elem={curDeckElements} deck={curDeck} perm={owner.username === username? true : false}/></div>
          {/* <Carousel word={curDeckElements} curDeck={curDeck} /> */}

          {curDeckElements.length > 0 && (
            <div className="deck-action--button">
              <Button
                className="test--button"
                variant="outline-secondary"
                href={`../deck/practice?deckID=${data.id}`}
              >
                {' '}
                Practice with this deck{' '}
              </Button>
              <Button
                className="test--button"
                variant="outline-info"
                // href={``}  // duplicate api
                onClick={handleDuplicate}
              >
                {' '}
                Duplicate this deck{' '}
              </Button>
              <Button
                className="test--button"
                variant="outline-danger"
                href={`../deck/game?deck=${data.id}`} // duplicate api
              >
                {' '}
                Play game{' '}
              </Button>
            </div>
          )}
        </Grid>
        <Grid item>
          <UserCard user={owner} />
        </Grid>
      </div>
    </div>
  )
}
