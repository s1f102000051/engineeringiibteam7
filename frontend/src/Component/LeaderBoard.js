import React from 'react'
import NavigationBar from './NavigationBar'
import {useState, useEffect, Suspense} from 'react'
import {Grid, Button} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import {Typography} from '@material-ui/core'
import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import GroupIcon from '@mui/icons-material/Group'
import SchoolIcon from '@mui/icons-material/School'
import {getCookie} from '../helper'
import LoadingButton from '@mui/lab/LoadingButton'
const LeaderBoardDeck = React.lazy(() => import('./LeaderBoardDeck'))
const LeaderBoardUser = React.lazy(() => import('./LeaderBoardUser'))
import CircularProgress from '@mui/material/CircularProgress'
import LinearProgress from '@mui/material/LinearProgress'

export default function LeaderBoard() {
  const [topuser, settopuser] = useState([])
  const [topuserElements, settopuserElements] = useState()
  // This is for lazy loading.
  const [userPage, setuserPage] = useState(1)
  const [isFetchingUser, setisFetchingUser] = useState(0)
  const [deckPage, setdeckPage] = useState(1)
  const [isFetchingDeck, setisFetchingDeck] = useState(0)
  const [topdeck, settopdeck] = useState([])
  const [topdeckElements, settopdeckElements] = useState()
  const [view, setview] = useState(0)
  const fetchUserData = async () => {
    // use for testing
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        userPage: userPage,
      }),
    }
    fetch('/api/user-leaderboard/', requestOptions)
      .then((res) => res.json())
      .then((data) => {
        setuserPage(userPage + 1)
        settopuser([...topuser, ...data['data']])
      })
  }
  const fetchDeckData = async () => {
    // use for testing
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        deckPage: deckPage,
      }),
    }
    setTimeout(async () => {
      const result = await fetch(`/api/deck-leaderboard/`, requestOptions)
      const data = await result.json()
      setdeckPage(deckPage + 1)
      settopdeck([...topdeck, ...data['data']])
      // setmaxDeckPage(data['page_num'])
      // maxDeckPage = data['page_num']
    }, 0)
  }
  const fetchMoreUserData = () => {
    setTimeout(() => {
      fetchUserData()
      setisFetchingUser(false)
    }, 1000)
  }
  const fetchMoreDeckData = () => {
    setTimeout(() => {
      fetchDeckData()
      setisFetchingDeck(false)
    }, 1000)
  }
  // whenever is fetching == true -> call fetchMoreUserData
  useEffect(() => {
    if (isFetchingUser) fetchMoreUserData()
  }, [isFetchingUser])
  useEffect(() => {
    if (isFetchingDeck) fetchMoreDeckData()
  }, [isFetchingDeck])
  // Check if we have already handle everything.
  const handleSroll = () => {
    if (window.innerHeight + window.scrollY >= document.body.scrollHeight) {
      setisFetchingUser(true)
    }
    if (window.innerHeight + window.scrollY >= document.body.scrollHeight) {
      setisFetchingDeck(true)
    }
  }
  useEffect(() => {
    fetchUserData()
    fetchDeckData()
    window.addEventListener('scroll', handleSroll)
  }, [])
  useEffect(() => {
    settopuserElements(
      topuser.map((user, index) => {
        return (
          <Suspense fallback={<div>Hello there</div>}>
            <LeaderBoardUser user={user} index={index}></LeaderBoardUser>
          </Suspense>
        )
      }),
    )
  }, [topuser])
  useEffect(() => {
    settopdeckElements(
      topdeck.map((deck, index) => {
        return (
          <Suspense
            fallback={
              // Somehow this shit didn't work.
              <div>Hello there</div>
            }
          >
            <LeaderBoardDeck deck={deck} index={index}></LeaderBoardDeck>
          </Suspense>
        )
      }),
    )
  }, [topdeck])
  return (
    <div>
      <NavigationBar></NavigationBar>
      <div className="content">
        <Grid style={{border: '2px', width: '30%'}}>
          <h1 className="text-4xl md:text-6xl font-bold title">Popular</h1>
          <div className="introduction">
            <div className={`homepage-intro${view == '0' ? '--select' : ''}`}>
              <Grid
                align="center"
                style={{width: '100%'}}
                onClick={() => {
                  setview(0)
                }}
              >
                <GroupIcon />
                <Typography variant="h6">TOP USERS</Typography>
                {view == '0' ? (
                  <Fade>
                    <div>
                      This is the hall of fame for users who contributes the most to our community
                    </div>
                  </Fade>
                ) : (
                  <div></div>
                )}
              </Grid>
            </div>

            <div className={`homepage-intro${view == '1' ? '--select' : ''}`}>
              <Grid
                align="center"
                style={{width: '100%'}}
                onClick={() => {
                  setview(1)
                }}
              >
                <SchoolIcon />
                <Typography variant="h6">TOP DECKS</Typography>
                {view == '1' ? (
                  <Fade>
                    <div>The most fun deck to learn.</div>
                  </Fade>
                ) : (
                  <div></div>
                )}
              </Grid>
            </div>
          </div>
        </Grid>
        {view == '1' && (
          <Grid container style={{width: '50%'}} className="leaderboard">
            <Button
              style={{width: '100%', backgroundColor: '#2C2E3A', color: 'white', height: '50px'}}
              className="leaderboard-top"
            >
              <Grid item xs={2}>
                #
              </Grid>
              <Grid item xs={4}>
                Deck
              </Grid>
              <Grid item xs={3}>
                Duplicate time
              </Grid>
              <Grid item xs={3}>
                Owner
              </Grid>
            </Button>
            {topdeckElements}
            {isFetchingDeck != 0 && (
              <LinearProgress style={{width: '100%', margin: '10px'}}></LinearProgress>
            )}
          </Grid>
        )}
        {view == '0' && (
          <Grid container style={{width: '50%'}} className="leaderboard">
            <Button
              style={{width: '100%', backgroundColor: '#2C2E3A', color: 'white', height: '50px'}}
              className="leaderboard-top"
            >
              <Grid item xs={2}>
                #
              </Grid>
              <Grid item xs={4}>
                User
              </Grid>
              <Grid item xs={3}>
                Duplicate time
              </Grid>
              <Grid item xs={3}>
                Level
              </Grid>
            </Button>
            {topuserElements}
            {isFetchingUser != 0 && (
              <LinearProgress
                style={{width: '100%', color: '#FFC106 !important', marginBottom: '7px'}}
              ></LinearProgress>
            )}
          </Grid>
        )}
      </div>
    </div>
  )
}
