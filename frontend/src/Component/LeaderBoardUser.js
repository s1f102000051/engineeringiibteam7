import React from 'react'
import {Button, Grid} from '@material-ui/core'
import {useHistory} from 'react-router-dom'

export default function LeaderBoardUser(props) {
  const history = useHistory()
  return (
    <Button style={{width: '100%', textTransform: 'lowercase', borderTop: '1px #808080 solid'}}>
      <Grid item xs={2}>
        {props.index + 1}
      </Grid>
      <Grid item xs={1} key={props.user.username}>
        <img
          src={'..' + props.user.avatar}
          alt={props.user.username}
          onError={({currentTarget}) => {
            currentTarget.onerror = null // prevents looping
            currentTarget.src =
              '/static/avatar_default.jpeg'
          }}
          height="50px"
          width="50px"
          style={{borderRadius: '50%'}}
          onClick={() => {
            history.push('/member?username=' + props.user.username)
          }}
        />
      </Grid>
      <Grid
        item
        xs={3}
        onClick={() => {
          history.push('/member?username=' + props.user.username)
        }}
      >
        {props.user.username}
      </Grid>
      <Grid item xs={3}>
        {props.user.duplicate_time}
      </Grid>
      <Grid item xs={3}>
        {props.user.experience_level}
      </Grid>
    </Button>
  )
}
