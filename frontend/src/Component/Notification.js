import {store} from 'react-notifications-component'

// https://www.npmjs.com/package/react-notifications-component

//Default notification format
let notification = {
    title: "Success", 
    message: "",
    type: "success",//success    danger    info    default    warning
    insert: "bottom",
    container: "bottom-right",
    animationIn: ["animate__animated", "animate__fadeIn"],
    animationOut: ["animate__animated", "animate__fadeOut"],
    pauseOnHover: "true",
    dismiss: {
      duration: 3000,
      pauseOnHover: true,
      click: true,
      showIcon: true,
    }
  }

function Notification(props) {
    function addNotification() {
        store.addNotification({
          ...notification,
          ...props
        })
      }
    addNotification() 
}

export default Notification
