import React from 'react'
import {useHistory} from 'react-router-dom'
import {getCookie} from '../helper'
import { Button } from '@material-ui/core'
import LogoutIcon from '@mui/icons-material/Logout';
export default function Logout(props) {
  const history = useHistory()
  function onLogout() {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
    }
    fetch('/rest-auth/logout/', requestOptions)
      .then((res) => res.json())
      .then((data) => {
        localStorage.clear()
        console.log(data)
        props.onLogout()
        history.push('/')
        history.go(0)
      })
  }
  return (
      <Button style={{margin: '5px', position: 'absolute', right: '0'}}  onClick={() => onLogout()}>
        <LogoutIcon/>
      </Button>
  )
}
