from django.shortcuts import render

# Create your views here.
def index(request, *args, **kwargs):
    packet= {
        'header' : 'Game | JayDict',
    }
    return render(request, 'frontend/index.html', packet)

