from django.shortcuts import render

# Create your views here.
def index(request, *args, **kwargs):
    packet= {
        'header' : 'View deck | JayDict',
    }
    return render(request, 'frontend/index.html', packet)

def practice(request, *args, **kwargs):
    packet= {
        'header' : 'Practice| JayDict',
    }
    return render(request, 'frontend/index.html', packet)
