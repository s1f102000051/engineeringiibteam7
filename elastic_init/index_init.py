from elasticsearch import Elasticsearch, RequestsHttpConnection
import json
import uuid
import re
import codecs
import random
import datetime
from dateutil import parser
import time

es = Elasticsearch()
def index():
    es.indices.delete(index='words', ignore=[400, 404])
    # nhớ create lúc đầu
    mapping = {
        "mappings": {
            "properties": {
                "id": {
                    "type": "text" # formerly "string"
                },
                "kanji": {
                    "type": "nested",
                    "properties" :{
                        "common":{
                            "type": "boolean"
                        },
                        "text":{
                            "type": "text",
                            "null_value": "NULL" 
                        },
                        "tags": {
                            "type": "nested",
                            "null_value": "NULL" 
                        }
                    },
                    "null_value": "NULL", 
                },
                "kana": {
                    "type": "nested",
                    "properties" :{
                        "common":{
                            "type": "boolean",
                            "null_value": "NULL" 
                        },
                        "text":{
                            "type": "text",
                            "null_value": "NULL" 
                        },
                        "tags": {
                            "type": "nested",
                            "null_value": "NULL" 
                        },
                        "appliesToKanji" : {
                            "type": "nested",
                            "null_value": "NULL" 
                        }
                    }
                },
                "sense": {
                    "type": "nested",
                    "properties" :{
                        "partOfSpeech":{
                            "type": "nested"
                        },
                        "appliesToKanji":{
                            "type": "text"
                        },
                        "appliesToKana": {
                            "type": "nested"
                        },
                        "related" : {
                            "type": "nested"
                        },
                        "field":{
                            "type": "nested"
                        },
                        "dialect":{
                            "type": "nested"
                        },
                        "misc": {
                            "type": "nested"
                        },
                        "info" : {
                            "type": "nested"
                        },
                        "languageSource":{
                            "type": "nested"
                        },
                        "gloss": {
                            "type": "nested",
                            "properties": {
                                "type":{
                                    "type": "text"
                                },
                                "lang": {
                                    "type": "text"
                                },
                                "text" : {
                                    "type": "text"
                                },
                            }
                        }
                    }
                }
            }
        }
    }
    es.indices.create(index='words', body= mapping, ignore=400)
    
    with open('database.json', 'r', encoding='utf-8') as read_file:
        data = read_file.readlines()
        for line in (data):
            document = json.loads(line)
            # for i in document:
            #     print(i)
            # print(type(document))
            # print(line)
            try:
                es.index(index='words', id = document['id'], document = document)
            except:
                pass
            print("Done reading json file ", document['id'])
            # break
index()
def search(txt):
    # query_body = {
    #     "query": {
    #         "match": {
    #             "kanji": {
    #                 "prefix_length": 1,
    #                 "query": 'そぞろ歩き',
    #                 "fuzziness": 2
    #             }
    #         }
    #         },
    #     "size" : 100
    # }

    query_body = {
        "query" : {
            "match" : {
                "kana.text" :txt
            },
            
        },
    }
    ans = es.search(index="words", body=query_body)['hits']['hits']
    for i in range(3):
        print(ans[i]["_index"])
        print('-------------------------------------------\n')
        
    # print(ans)




# search('ぴり')