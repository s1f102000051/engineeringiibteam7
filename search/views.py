from operator import truediv
from time import time
from django.http.response import JsonResponse
from elasticsearch import Elasticsearch
from rest_framework import generics, serializers, status
from django.http import JsonResponse, Http404
from dateutil import parser
from django.shortcuts import render, redirect
from datetime import datetime

import urllib
import urllib.parse
import json
import random

# prev_time=0

try: #run only once
    es = Elasticsearch()
    print("Checking Elasticsearch")
    
    query_body_kanji_test = {
            "query" : {
                "match" : {
                    "kanji.text" :'し'
                },
                
            },
        } 
    es.search(index="words", body=query_body_kanji_test)['hits']['hits']
except:
    es=None
    print("Elastic server is not found!")

# def changeElasticLink(link):
#     global es
#     if (link != None):
#         es = Elasticsearch(link)
#         print("Cur link: ",link)

def search(request):
    if 'word' in request.GET:
        q = request.GET.get('word')    
        print("Request GET: ", request.GET)
    else: # if the query is empty
        return JsonResponse({}, status = status.HTTP_200_OK)
        

    # now = datetime.now().strftime('%S.%f')[:-4]
    # global prev_time
    # print("Current Time = ", now)
    # now=float(now)
    # print("Prev time = ", prev_time)
    # time_distance = now-prev_time
    # if time_distance <0 :
    #     time_distance = 60+ time_distance
    # print("Time distance: ", time_distance)
    # prev_time=now
    
    # if es!=None: # use Elasticsearch
    #     # changeElasticLink(request.session['elastic_link'])
        
        
        
        

    #     query_body_kanji = {
    #         "query" : {
    #             "match" : {
    #                 "kanji.text" :q
    #             },
                
    #         },
    #     }

    #     query_body_kana = {
    #         "query" : {
    #             "match" : {
    #                 "kana.text" :q
    #             },
                
    #         },
    #     }
    #     if q:
    #         ans_kanji = es.search(index="words", body=query_body_kanji)['hits']['hits']
    #         ans_kana = es.search(index="words", body=query_body_kana)['hits']['hits']
    #     else:
    #         ans_kanji = []
    #         ans_kana =[]

    #     packet = []
    #     for i in range(min(3,len(ans_kanji))):
    #         meanings=[]
    #         for j in ans_kanji[i]["_source"]["sense"][0]["gloss"]:
    #             meanings.append(j["text"])
    #         word_info = {'text':ans_kanji[i]["_source"]["kanji"][0]["text"], 'text_kanji':ans_kanji[i]["_source"]["kanji"][0]["text"] ,  'gloss':meanings, 'score': ans_kanji[i]["_score"]}
    #         try:
    #             word_info['text_kana'] =  ans_kanji[i]["_source"]["kana"][0]["text"]
    #         except:
    #             print("no kana for ", word_info['text'])
    #         packet.append(word_info)
    #     for i in range(min(3,len(ans_kana))):
    #         meanings=[]
    #         for j in ans_kana[i]["_source"]["sense"][0]["gloss"]:
    #             meanings.append(j["text"])
    #         word_info = {'text':ans_kana[i]["_source"]["kana"][0]["text"], 'text_kana':ans_kana[i]["_source"]["kana"][0]["text"],  'gloss':meanings, 'score': ans_kanji[i]["_score"]}
    #         try:
    #             word_info['text_kanji'] =  ans_kana[i]["_source"]["kanji"][0]["text"]
    #         except:
    #             print("no kanji for ", word_info['text'])
    #         packet.append(word_info)
        
    #     print("q: ",q)
    #     print("Packet return from Elasticsearch: ",packet,"\n")

    #     return JsonResponse({'ans': packet}, status = status.HTTP_200_OK)
    # else: # use API
    print("Elasticsearch server is not found, using API instead\n")
    packet = []

    jisho_api_url = "https://jisho.org/api/v1/search/words?keyword={}".format(
    urllib.parse.quote(q))
    req = urllib.request.Request(jisho_api_url)
    res = urllib.request.urlopen(req)
    data = json.loads(res.read().decode('utf-8'))['data']
    res.close()

    # take　all the words.
    score=0 # score does not matter while using jisho API
    for i in range(len(data)):
        word = {
            'level': 'unknown',
            'meaning': data[i]['senses'][0]['english_definitions'],
            
        }
        try:
            word['reading']=  data[i]['japanese'][0]['reading']
        except:
            continue
        try:
            word['word']=  data[i]['japanese'][0]['word']
        except:
            word['word']= data[i]['slug']
        # check for katakana content
        # if yes, set it to reading.
        # for c in word['word']:
        #     if c.isdigit():
        #         word['word'] = word['reading']
        #         break
        if len(data[i]['jlpt']):
            word['level'] = data[i]['jlpt'][0]
        else:
            word['level'] = "N/A"
        # meaning = ','.join(word['meaning'])
        # return (word['word'], word['level'].upper(), meaning, word['reading'], image_url)
        # print (word['word'], word['level'].upper(), meaning, word['reading'])
        word_info = {'text':word['word'], 'text_kana':word['reading'],  'gloss':word['meaning'], 'score': score, 'jlpt_level': word['level']}
        score-=1
        packet.append(word_info)
    print("q: ",q,"\n")
    print("Packet return from API: ",packet,"\n")
    return JsonResponse({'ans': packet, 'api': True}, status = status.HTTP_200_OK)

